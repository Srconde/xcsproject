package es.uvigo.esei.xcsproject.entities;


import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.util.Date;

public class PersonTest {

    private String name;
    private String surName;

    private String newName;
    private String newSurName;

    @Before
    public void setUp(){
        this.name = "Pepe";
        this.surName = "Perez";

        this.newName = "Rodrigo";
        this.newSurName = "Rodriguez";

    }
    

    @Test
    public void testSetName(){
        final Person newPerson = new Person(name, surName, new Date());
        newPerson.setName(newName);
        assertThat (newPerson.getName(), is(equalTo(newName)));
    }
}
