package es.uvigo.esei.xcsproject.entities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(
        PersonTest.class
)
public class EntitiesTestSuite {}
