CREATE DATABASE IF NOT EXISTS `xcsproject`
DEFAULT CHARACTER SET = utf8mb4
DEFAULT COLLATE utf8mb4_unicode_ci;

USE `xcsproject`;

--
-- User creation
--
GRANT ALL PRIVILEGES ON xcsproject.* TO xcsuser@localhost IDENTIFIED BY 'xcspass';
FLUSH PRIVILEGES;
